import firebase from "firebase"
import "firebase/auth"
// import withFirebaseAuth from "react-with-firebase-auth";

const app = firebase.initializeApp({
  apiKey: "AIzaSyDaAE2vPZGbkTj3-tBVUWdsKIn-8QvJONQ",
  authDomain: "simple-react-app-a6408.firebaseapp.com",
  databaseURL: "https://simple-react-app-a6408-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "simple-react-app-a6408",
  storageBucket: "simple-react-app-a6408.appspot.com",
  messagingSenderId: "782224499628",
  appId: "1:782224499628:web:1d6ea4c46dae0be0fbd009"
})

export const auth = app.auth()
// const firebaseAppAuth = app.auth();
// const providers = {
//   googleProvider: new firebase.auth.GoogleAuthProvider(),
// };

// export default withFirebaseAuth({
//   providers,
//   firebaseAppAuth,
// })(firebase);

export default app
