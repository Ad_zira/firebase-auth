import React, { useContext, useState, useEffect } from "react"
import withFirebaseAuth from "../firebase"

const AuthContext = React.createContext()

export function useAuth() {
  return useContext(AuthContext)
}

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState()
  const [loading, setLoading] = useState(true)

  function signup(email, password) {
    return withFirebaseAuth.auth().createUserWithEmailAndPassword(email, password)
  }

  function login(email, password) {
    withFirebaseAuth.auth().signInWithEmailAndPassword(email, password).then((test) => console.log(test))
    //console.log(data)
  }

  function logout() {
    return withFirebaseAuth.auth.signOut()
  }

  function resetPassword(email) {
    return withFirebaseAuth.auth().sendPasswordResetEmail(email)
  }

  function updateEmail(email) {
    return currentUser.updateEmail(email)
  }

  function updatePassword(password) {
    return currentUser.updatePassword(password)
  }

  useEffect(() => {
    const unsubscribe = withFirebaseAuth.auth.onAuthStateChanged(user => {
      setCurrentUser(user)
      setLoading(false)
    })

    return unsubscribe
  }, [])

  const value = {
    currentUser,
    login,
    signup,
    logout,
    resetPassword,
    updateEmail,
    updatePassword
  }

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  )
}
